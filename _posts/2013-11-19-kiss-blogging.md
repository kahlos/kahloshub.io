---
layout: post
title: "KISS Blogging Methods"
description: "Improving personal blog usefulness through consistent writing method"
category: articles
tags: [blogging, blog, kiss, simple, concise, occam's razor, consistent]
comments: true
---

Numerous quality blogs undermine the infinity of personal blogs. Using a consistent, KISS principled, search orientated writing method I hope to improve the usefulness of my writing. 

**KISS** - Keep It Simple, Stupid

> A principle akin to occam's razor; the simplest solution is the best solution.

When I visit a blog, I'll be seeking specific information most of the time. I don't care about a personal story, I don't want further information; I will filter through to find my search engine queries. For a personal blog to be useful for the motives of a typical reader it must clearly outline its information, quickly.

To this avail I'll be adhering to the KISS principle and applying a consistent writing method throughout future posts:

1. Abstract style first paragraph
2. Explanation of title and description
3. Motives for post
4. Post body
5. Conclusions

Structuring posts in this way, along with a concise writing style, should allow the typical search engine sourced reader to find the precise content as quickly as possible.
No ramblings, no block text mess, KISS.
